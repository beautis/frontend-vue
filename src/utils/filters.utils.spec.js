import { cleanFilters, toQueryString } from './filters.utils'

describe('Filters utils', () => {

  test('clean filters', () => {
    const filters = {
      name: 'Lamer',
      email: '',
    }

    const res = cleanFilters(filters)
    expect(res).toEqual({
      name: 'Lamer',
    })
  })

  test('default prefix to query string', () => {
    const filters = {
      offset: 0,
      limit: 25,
      name: 'Lamer',
      email: 'ts@gmail.com',
    }

    const res = toQueryString(filters)
    expect(res).toBe('?offset=0&limit=25&name=Lamer&email=ts@gmail.com')
  })

  test('custom prefix to query string', () => {
    const filters = {
      name: 'Lamer',
      email: 'ts@gmail.com',
    }

    const res = toQueryString(filters, '&')
    expect(res).toBe('&name=Lamer&email=ts@gmail.com')
  })

  test('empty to query string', () => {
    const filters = {}

    const res = toQueryString(filters, '&')
    expect(res).toBe('')
  })
})
