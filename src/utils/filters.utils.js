export const cleanFilters = (filters) => {
  return Object.keys(filters).reduce((prev, key) => {
    if (filters[key]) {
      return { ...prev, [key]: filters[key] }
    }

    return prev
  }, {})
}

export const toQueryString = (filters, prefix = '?') => {
  const keys = Object.keys(filters)

  if (keys.length < 1) {
    return ''
  }

  return prefix + keys.map(key => key + '=' + filters[key]).join('&')
}
