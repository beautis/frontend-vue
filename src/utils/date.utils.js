import * as moment from 'moment'

export function formatDate (date, format = 'DD.MM.YYYY') {
  if (!date) {
    return ''
  }

  return moment(date).format(format)
}

export function getAge (date) {
  if (!date) {
    return ''
  }

  return moment().diff(date, 'years', false)
}
