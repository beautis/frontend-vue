import { mount } from '@vue/test-utils'

import Button from './BaseButton'

describe('Button Component', () => {

  test('test handle click', () => {
    const buttonWrapper = mount(Button)
    buttonWrapper.trigger('click')

    expect(buttonWrapper.emitted().outClick).toBeTruthy()
  })

  test('small button true', () => {
    const buttonWrapper = mount(Button, {
      propsData: {
        small: true,
      },
    })

    expect(buttonWrapper.classes('button--small')).toBe(true)
  })

  test('is not rounded be default', () => {
    const buttonWrapper = mount(Button)

    expect(buttonWrapper.classes('button--small')).toBe(false)
  })

  test('round class', () => {
    const buttonWrapper = mount(Button, {
      propsData: {
        round: true,
      },
    })

    expect(buttonWrapper.classes('button--rounded')).toBe(true)
  })

  test('is not round by default', () => {
    const buttonWrapper = mount(Button)

    expect(buttonWrapper.classes('button--rounded')).toBe(false)
  })

  test('colors', () => {
    const colors = [
      'dark',
      'light',
      'red',
      'orange',
    ]

    for (const color of colors) {
      const buttonWrapper = mount(Button, {
        propsData: {
          color,
        },
      })

      expect(buttonWrapper.classes(`button--${color}`)).toBe(true)
    }
  })
})
