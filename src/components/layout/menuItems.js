export const menuItems = {
  top: [
    {
      url: 'clients/list',
      label: 'Clients',
      icon: 'people',
      active: true,
      children: [
        {
          url: 'clients',
          label: 'Create client',
        },
      ],
    },
  ],
  middle: [
    {
      url: 'orders',
      label: 'Orders',
      icon: 'schedule',
      active: false,
      children: [
        {
          url: 'visit',
          label: 'Create visit',
        },
        {
          url: 'date',
          label: 'Create date',
        },
      ],
    },
    {
      url: 'sales',
      label: 'Sale',
      icon: 'shopping_cart',
      active: false,
      children: [
        {
          url: 'list',
          label: 'List of sales',
        },
        {
          url: 'create',
          label: 'Create sale',
        },
      ],
    },
    {
      url: 'store',
      label: 'Store',
      icon: 'store_mall_directory',
      active: false,
    },
  ],
  bottom: [
    {
      url: 'calendar',
      label: 'Calendar',
      icon: 'event',
    },
    {
      url: 'service',
      label: 'Services',
      icon: 'room_service',
      active: false,
    },
    {
      url: 'marketing',
      label: 'Marketing',
      icon: 'campaign',
      active: false,
    },
    {
      url: 'staff',
      label: 'Staff',
      icon: 'engineering',
      active: false,
    },
    {
      url: 'communication',
      label: 'Communication',
      icon: 'phone',
      active: false,
    },
    {
      url: 'settings',
      label: 'Settings',
      icon: 'settings',
      active: false,
    },
  ],
}
