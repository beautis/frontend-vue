import { createLocalVue, mount, RouterLinkStub, shallowMount } from '@vue/test-utils'
import VueRouter from 'vue-router'

import MenuItem from './MenuItem'

describe('Menu item component', () => {

  const localVue = createLocalVue()
  localVue.use(VueRouter)
  const router = new VueRouter()

  const menuItem = {
    url: 'orders',
    label: 'Orders',
    redirect: 'clients',
    icon: 'schedule',
    active: false,
  }

  test('click event', () => {
    const menuItemWrapper = mount(MenuItem, {
      stubs: {
        RouterLink: RouterLinkStub,
      },
    })
    menuItemWrapper.trigger('click')

    expect(menuItemWrapper.emitted().outClick).toBeTruthy()
  })

  test('url with redirect', () => {
    const menuItemWrapper = shallowMount(MenuItem, {
      localVue,
      router,
      propsData: {
        menuItem,
      },
    })

    expect(menuItemWrapper.find('router-link-stub').attributes('to')).toBe('/orders/clients')
  })

  test('url without redirect', () => {
    const copyMenuItem = {
      ...menuItem,
    }

    delete copyMenuItem.redirect

    const menuItemWrapper = shallowMount(MenuItem, {
      localVue,
      router,
      propsData: {
        menuItem: copyMenuItem,
      },
    })

    expect(menuItemWrapper.find('router-link-stub').attributes('to')).toBe('/orders')
  })

  test('show label by default', () => {
    const menuItemWrapper = mount(MenuItem, {
      propsData: {
        menuItem,
      },
      stubs: {
        RouterLink: RouterLinkStub,
      },
    })

    expect(menuItemWrapper.find('a').text()).toBe('Orders')
  })

  test('show label false', () => {
    const menuItemWrapper = mount(MenuItem, {
      propsData: {
        showLabel: false,
      },
      stubs: {
        RouterLink: RouterLinkStub,
      },
    })

    expect(menuItemWrapper.find('.link-wrapper').exists()).toBeFalsy()
  })
})
