import { mount, RouterLinkStub } from '@vue/test-utils'

import MenuItem from './MenuItem'

describe('Submenu Item component', () => {

  test('click event', () => {
    const submenuItemWrapper = mount(MenuItem, {
      stubs: {
        RouterLink: RouterLinkStub,
      },
    })

    submenuItemWrapper.trigger('click')

    expect(submenuItemWrapper.emitted().outClick).toBeTruthy()
  })
})
