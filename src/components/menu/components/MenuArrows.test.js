import { mount, RouterLinkStub } from '@vue/test-utils'

import MenuArrows from './MenuArrows'

describe('Menu Arrows component', () => {

  test('click event', () => {
    const menuArrowsWrapper = mount(MenuArrows, {
      stubs: {
        RouterLink: RouterLinkStub,
      },
    })

    menuArrowsWrapper.trigger('click')

    expect(menuArrowsWrapper.emitted().outClick).toBeTruthy()
  })
})
