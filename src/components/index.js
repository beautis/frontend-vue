import Vue from 'vue'
import BaseButton from './button/BaseButton'
import BaseInput from './input/BaseInput'
import BaseLayout from './layout/BaseLayout'
import BaseLayoutProtected from './layout/BaseLayoutProtected'
import BaseTableNormal from './table/table-normal/BaseTableNormal'
import BaseCircle from './circle/BaseCircle'
import BaseTableCard from './table/table-card/BaseTableCard'
import BaseContentWrapper from './content/wrappers/BaseContentWrapper'
import BaseTopContent from './content/BaseTopContent'
import BaseIconWrapper from './content/BaseIconWrapper'
import BaseMenu from './menu/BaseMenu'
import BaseMenuMobile from './menu/BaseMenuMobile'
import BaseNotFound from './common/BaseNotFound'
import BaseSelect from './select/BaseSelect'
import BaseCheckbox from './checkbox/BaseCheckbox'
import BaseTableWrapper from './table/BaseTableWrapper'
import BaseTabs from './tabs/BaseTabs'
import BaseLoader from './loader/BaseLoader'
import BaseBreadcrumbs from './breacrumbs/BaseBreadcrumbs'
import BaseSectionHeader from './layout/BaseSectionHeader'
import BaseLinkButton from './button/BaseLinkButton'
import BaseTableLayout from './layout/BaseTableLayout'
import BaseFilterWrapper from './filters/BaseFilterWrapper'
import BaseModal from './modal/BaseModal'

Vue.component('Button', BaseButton)
Vue.component('Input', BaseInput)
Vue.component('Layout', BaseLayout)
Vue.component('LayoutProtected', BaseLayoutProtected)
Vue.component('TableLayout', BaseTableLayout)
Vue.component('TableNormal', BaseTableNormal)
Vue.component('TableCard', BaseTableCard)
Vue.component('TableWrapper', BaseTableWrapper)
Vue.component('Menu', BaseMenu)
Vue.component('MenuMobile', BaseMenuMobile)
Vue.component('Select', BaseSelect)
Vue.component('Checkbox', BaseCheckbox)
Vue.component('Tabs', BaseTabs)
Vue.component('Loader', BaseLoader)
Vue.component('Breadcrumbs', BaseBreadcrumbs)
Vue.component('SectionHeader', BaseSectionHeader)
Vue.component('LinkButton', BaseLinkButton)
Vue.component('FilterWrapper', BaseFilterWrapper)
Vue.component('Modal', BaseModal)

Vue.component('CircleElement', BaseCircle)
Vue.component('ContentWrapper', BaseContentWrapper)
Vue.component('TopContent', BaseTopContent)
Vue.component('IconWrapper', BaseIconWrapper)
Vue.component('NotFound', BaseNotFound)
