import Vue from 'vue'
import { mount } from '@vue/test-utils'

import Pagination from './Pagination'

describe('Pagination component', () => {

  test('default props', () => {
    const paginationWrapper = mount(Pagination)

    expect(paginationWrapper.props('page')).toBe(0)
    expect(paginationWrapper.props('perPage')).toBe(25)
    expect(paginationWrapper.props('count')).toBe(0)
  })

  test('max page', done => {
    const paginationWrapper = mount(Pagination, {
      propsData: {
        count: 100,
        perPage: 10,
      },
    })

    Vue.nextTick(() => {
      expect(paginationWrapper.vm.maxPage).toBe(10)
      done()
    })
  })

  test('left button disabled', () => {
    const paginationWrapper = mount(Pagination, {
      propsData: {
        count: 100,
        perPage: 10,
        page: 0,
      },
    })

    expect(paginationWrapper.find('svg:first-child').classes('disabled')).toBe(true)
  })

  test('left button is not disabled', () => {
    const paginationWrapper = mount(Pagination, {
      propsData: {
        count: 100,
        perPage: 10,
        page: 5,
      },
    })

    expect(paginationWrapper.find('svg:first-child').classes('disabled')).toBe(false)
  })

  test('right button disabled', () => {
    const paginationWrapper = mount(Pagination, {
      propsData: {
        count: 100,
        perPage: 10,
        page: 9,
      },
    })

    expect(paginationWrapper.find('svg:last-child').classes('disabled')).toBe(true)
  })

  test('right button is not disabled', () => {
    const paginationWrapper = mount(Pagination, {
      propsData: {
        count: 100,
        perPage: 10,
        page: 1,
      },
    })

    expect(paginationWrapper.find('svg:last-child').classes('disabled')).toBe(false)
  })
})
