import { mount } from '@vue/test-utils'

import Input from './BaseInput'

describe('Input component', () => {

  test('no placeholder by default', () => {
    const inputWrapper = mount(Input)

    expect(inputWrapper.find('input').attributes('placeholder')).toBe('')
  })

  test('placeholder value', () => {
    const placeholder = 'Please write a text'
    const inputWrapper = mount(Input, {
      propsData: {
        placeholder,
      },
    })

    expect(inputWrapper.find('input').attributes('placeholder')).toBe(placeholder)
  })

  test('type value by default', () => {
    const inputWrapper = mount(Input)

    expect(inputWrapper.find('input').attributes('type')).toBe('text')
  })

  test('type', () => {
    const type = 'password'
    const inputWrapper = mount(Input, {
      propsData: {
        type,
      },
    })

    expect(inputWrapper.find('input').attributes('type')).toBe(type)
  })

  test('value by default', () => {
    const inputWrapper = mount(Input)

    expect(inputWrapper.find('input').element.value).toBe('')
  })

  test('value', () => {
    const value = 'Some value'
    const inputWrapper = mount(Input, {
      propsData: {
        value,
      },
    })

    expect(inputWrapper.find('input').element.value).toBe(value)
  })
})
