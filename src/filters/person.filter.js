export function gender (value) {
  if (value === 1) {
    return 'Male'
  }

  return 'Female'
}
