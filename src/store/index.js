import Vue from 'vue'
import Vuex from 'vuex'

import { clientsListStore } from '../modules/clients/client-list/clients-list.store'
import { coreStore } from '../modules/core/store/core.store'
import { uiStore } from '../modules/core/store/ui.store'
import { ordersListStore } from '../modules/orders/store/orders.store'
import { clientDetailStore } from '../modules/clients/client-detail/client-detail.store'

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    modules: {
      core: coreStore,
      ui: uiStore,
      clients: clientsListStore,
      clientDetail: clientDetailStore,
      orders: ordersListStore,
    },
  })
}
