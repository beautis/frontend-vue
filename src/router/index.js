import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '../modules/auth/views/Login'
import ForgotPassword from '../modules/auth/views/ForgotPassword'
import SignUp from '../modules/auth/views/SignUp'
import BaseLayoutProtected from '../components/layout/BaseLayoutProtected'
import BaseNotFound from '../components/common/BaseNotFound'
import SalesListView from '../modules/sales/views/SalesListView'
import OrdersListView from '../modules/orders/views/OrdersListView'
import { clientRoutes } from '../modules/clients/client.routes'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    redirect: '/login',
  },
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/forgot-password',
    name: 'forgot-password',
    component: ForgotPassword,
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: SignUp,
  },
  ...clientRoutes,
  {
    path: '/sales',
    component: BaseLayoutProtected,
    children: [
      {
        path: '',
        name: 'sales',
        component: SalesListView,
      },
    ],
  },
  {
    path: '/orders',
    component: BaseLayoutProtected,
    children: [
      {
        path: '',
        name: 'orders',
        component: OrdersListView,
      },
    ],
  },
  {
    path: '**',
    component: BaseNotFound,
  },
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
