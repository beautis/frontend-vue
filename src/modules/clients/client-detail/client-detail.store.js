import { clientDetailApi } from './client-detail.api'

export const clientDetailStore = {
  namespaced: true,

  state () {
    return {
      client: undefined,
    }
  },

  getters: {
    clientGetter (state) {
      return state.client || {}
    },
  },

  mutations: {
    updateClient (state, payload) {
      state.client = payload.client
    },
  },

  actions: {
    async fetchClient ({ commit }, id) {
      const client = await clientDetailApi.get(id)

      commit({
        type: 'updateClient',
        client,
      })
    },
  },
}
