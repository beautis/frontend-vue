import { api } from '../../core/request/fetch'

export const clientDetailApi = {
  async get (id) {
    return api.get(`http://localhost:4001/api/v1/client/${id}`)
  },
}
