import { clientListApi } from './client-list.api'

export const startLoading = commit => {
  commit({
    type: 'setLoading',
    loading: true,
  })
}

export function changeExcelState (commit, props) {
  const { step, steps, status } = props

  commit('ui/excelState', {
    step,
    steps,
    status,
  }, { root: true })
}

export const finishedLoading = commit => {
  commit({
    type: 'setLoading',
    loading: false,
  })
}

export const clientsListStore = {
  namespaced: true,

  state () {
    return {
      list: [],
      count: 0,
      page: 0,
      loading: false,
      filters: {},
      excelData: [],
      cancel: false,
    }
  },

  getters: {
    listGetter (state) {
      return state.list
    },
    countGetter (state) {
      return state.count
    },
    pageGetter (state) {
      return state.page
    },
    loadingGetter (state) {
      return state.loading
    },
    filtersGetter (state) {
      return state.filters
    },
    excelDataGetter (state) {
      return state.excelData
    },
    cancelGetter (state) {
      return state.cancel
    },
  },

  mutations: {
    listUpdate (state, payload) {
      state.list = payload.list
      state.count = payload.count
    },
    pageUpdate (state, payload) {
      state.page = payload.page
    },
    setLoading (state, payload) {
      state.loading = payload.loading
    },
    setExcelData (state, payload) {
      state.excelData = payload.excelData
    },
    setCancelState (state, payload) {
      state.cancel = payload.cancel
    },
  },

  actions: {
    async generateExcel ({ commit, getters }, count) {
      const steps = Math.ceil(count / 100)
      const filters = getters.filtersGetter

      try {
        for (let page = 0; page < steps; page++) {
          const cancel = getters.cancelGetter
          if (cancel) {
            throw new Error()
          }

          changeExcelState(commit, { step: page + 1, steps, status: 'loading' })

          const response = await clientListApi.list({
            offset: 0,
            limit: 25,
            organizationsId: 183,
            ...filters,
          })

          commit({
            type: 'setExcelData',
            excelData: [...getters.excelDataGetter, ...response.data]
          })
        }

        changeExcelState(commit, { step: steps, steps, status: 'finished' })
      } catch (_) {
        commit({
          type: 'setCancelState',
          cancel: false,
        })
      }

    },

    async fetchClients ({ commit }, filters) {
      filters = filters || {}

      startLoading(commit)
      const response = await clientListApi.list({
        offset: 0,
        limit: 25,
        organizationsId: 183,
        ...filters,
      })

      commit({
        type: 'listUpdate',
        list: response.data,
        count: response.count,
        filters,
      })

      finishedLoading(commit)
    },

    updatePage ({ commit }, page) {
      commit({
        type: 'pageUpdate',
        page,
      })
    },

    cancel ({ commit }) {
      commit({
        type: 'setCancelState',
        cancel: true,
      })
    },
  },

}
