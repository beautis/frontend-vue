import email from 'vuelidate/lib/validators/email'

export const clientListForm = {
  defaultParams: {
    status: '1,-1',
    gender: '1,2',
    name: '',
    sign: '',
    email: '',
    phone: '',
  },
  validators: {
    status: {},
    gender: {},
    name: {},
    sign: {},
    email: { email },
    phone: {},
  }
}
