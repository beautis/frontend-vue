import { api } from '../../core/request/fetch'
import { toQueryString } from '@/utils/filters.utils'

export const clientListApi = {
  async list (params) {
    const query = toQueryString(params)

    return api.abortableGet(`http://localhost:4001/api/v1/clients${query}`).ready
  },
}
