import * as XLSX from 'xlsx'
import { saveAs } from 'file-saver'

import { formatDate } from '@/utils/date.utils'

const object = {
  data: [],
  sheetName: 'excel',

  init (data) {
    this.data = []
    const workbook = XLSX.utils.book_new()
    workbook.Props = data.props
    workbook.SheetNames.push(this.sheetName)

    return workbook
  },

  createBuffer (data) {
    const buf = new ArrayBuffer(data.length)
    const view = new Uint8Array(buf)
    for (let i = 0, length = data.length; i < length; i++) {
      view[i] = data.charCodeAt(i) & 0xFF
    }

    return buf
  },

  save (props) {
    console.log([props.headers, ...props.data])
    props.workBook.Sheets[this.sheetName] = XLSX.utils.aoa_to_sheet([props.headers, ...props.data])

    const workData = XLSX.write(props.workBook, { bookType: 'xlsx', type: 'binary' })
    const buffer = this.createBuffer(workData)
    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), props.fileName)
  },

  transform (data) {
    const flattened = []

    data.forEach(row => {
      const data = {
        ...row,
        gender: (row.gender == 2) ? 'Muž' : 'Žena',
        lastVisit: formatDate(row.lastVisit, 'DD.MM.YYYY  hh:mm:ss'),
        saintsDate: formatDate(row.saintsDate),
        birthDate: formatDate(row.birthDate),
      }

      flattened.push(Object.values(data))
    })

    return flattened
  },
}

export const clientListExcel = Object.create(object)
