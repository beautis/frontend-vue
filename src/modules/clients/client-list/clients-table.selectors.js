import FemaleIcon from '../../../assets/images/woman.svg'
import ManIcon from '../../../assets/images/man.svg'

export const clientsTableSelectors = [
  {
    selector: 'firstName',
  },
  {
    selector: 'lastName',
  },
  {
    selector: 'gender',
    type: 'custom',
    callback (row) {
      return (row.gender === 2) ? FemaleIcon : ManIcon
    },
  },
  {
    selector: 'cognitiveSign',
  },
  {
    selector: 'birthDate',
    type: 'date',
  },
  {
    selector: 'saintsDate',
    type: 'date',
  },
  {
    selector: 'email',
  },
  {
    selector: 'phone',
  },
  {
    selector: 'note',
  },
  {
    selector: 'lastVisit',
    type: 'datetime',
  },
]
