import BaseLayoutProtected from '../../components/layout/BaseLayoutProtected'

const ClientListView = () => import('./client-list/client-list.view')
const ClientDetailView = () => import('./client-detail/client-detail.view')
const ClientUpdateView = () => import('./client-update/ClientUpdate.view')

export const clientRoutes = [
  {
    path: '/clients',
    component: BaseLayoutProtected,
    children: [
      {
        path: '',
        name: 'clients',
        component: ClientListView,
      }, {
        path: ':id',
        name: 'client-list',
        component: ClientDetailView,
      },
      {
        path: ':id/update',
        name: 'client-update',
        component: ClientUpdateView,
      },
    ],
  },
]
