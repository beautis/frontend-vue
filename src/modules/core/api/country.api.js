import { api } from '../request/fetch'

export const countryApi = {
  async getCountries () {
    return await api.get('http://localhost:4002/api/v1/countries')
  },
}
