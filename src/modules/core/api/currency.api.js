import { api } from '../request/fetch'

export const currencyApi = {
  async getCurrencies () {
    return api.get('http://localhost:4002/api/v1/currencies')
  },
}
