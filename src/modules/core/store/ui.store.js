export const uiStore = {
  namespaced: true,

  state () {
    return {
      filtersOpen: false,
      modalOpen: false,
      excel: {
        step: 0,
        steps: 0,
        status: 'loading',
      },
      title: '',
    }
  },

  getters: {
    filtersOpenGetter (state) {
      return state.filtersOpen
    },
    modalOpenGetter (state) {
      return state.modalOpen
    },
    excelStateGetter (state) {
      return state.excel
    },
    titleGetter (state) {
      return state.title
    },
  },

  mutations: {
    filtersOpen (state, payload) {
      state.filtersOpen = payload.open
    },

    modalOpen (state, payload) {
      state.modalOpen = payload.open
    },

    excelState (state, payload) {
      state.excel = {
        ...state.excel,
        ...payload,
      }
    },

    setTitle (state, payload) {
      state.title = payload.title
    },
  },

  actions: {
    changeTitle ({ commit }, title) {
      commit({
        type: 'setTitle',
        title,
      })
    },

    changeFiltersOpen ({ commit }, open) {
      commit({
        type: 'filtersOpen',
        open,
      })
    },

    changeModalOpen ({ commit }, open) {
      commit({
        type: 'modalOpen',
        open,
      })
    },

    changeExcelState ({ commit }, excel) {
      commit({
        type: 'excelState',
        excel,
      })
    }
  },
}
