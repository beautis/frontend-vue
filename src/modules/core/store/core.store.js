import { countryApi } from '../api/country.api'
import { currencyApi } from '../api/currency.api'

export const coreStore = {
  namespaced: true,

  state () {
    return {
      countries: [],
      currencies: [],
    }
  },

  getters: {
    countryGetter (state) {
      return state.countries
    },
    currencyGetter (state) {
      return state.currencies
    },
  },

  mutations: {
    countriesUpdate (state, payload) {
      state.countries = payload.countries
    },
    currenciesUpdate (state, payload) {
      state.currencies = payload.currencies
    },
  },

  actions: {
    async fetchCountries ({ commit }) {
      const response = await countryApi.getCountries()
      commit({
        type: 'countriesUpdate',
        countries: response.data,
      })
    },
    async fetchCurrencies ({ commit }) {
      const response = await currencyApi.getCurrencies()
      commit({
        type: 'currenciesUpdate',
        currencies: response.data,
      })
    }
  },
}
