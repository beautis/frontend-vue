import * as XLSX from 'xlsx'
import { saveAs } from 'file-saver'

import { excelFormatter } from './excel.format'

const object = {
  data: [],
  sheetName: '',

  init (data) {
    this.data = []
    const workbook = XLSX.utils.book_new()
    workbook.Props = data.props
    workbook.SheetNames.push(this.sheetName)
    return workbook
  },
  createBuffer (workData) {
    const buf = new ArrayBuffer(workData.length)
    const view = new Uint8Array(buf)
    for (let i = 0, length = workData.length; i < length; i++) {
      view[i] = workData.charCodeAt(i) & 0xFF
    }
    return buf
  },
  save (name, workbook) {
    workbook.Sheets[this.sheetName] = XLSX.utils.aoa_to_sheet(this.data)
    excelFormatter.number(workbook.Sheets.excel)

    const workData = XLSX.write(workbook, { bookType: 'xlsx', type: 'binary' })
    const buffer = this.createBuffer(workData)
    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), name)
  }
}

export const excelGenerator = Object.create(object)
