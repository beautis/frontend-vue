import * as XLSX from 'xlsx'

const object = {
  number (excel, sign) {
    excel.v = excel.v.replace(sign, '')
    delete excel.w
    switch (sign) {
      case 'whole':
        excel.z = '0'
        break
      case '$':
        excel.z = '$0.00'
        break
      case '%':
        excel.z = '0.00%'
        excel.v = Number(excel.v) / 100
        break
      default:
        excel.z = '0.00'
    }
    excel.t = 'n'
    XLSX.utils.format_cell(excel)
  },
}

export const excelFormatter = Object.create(object)
