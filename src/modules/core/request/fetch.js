export function abortableFetch (request, options) {
  const controller = new AbortController()
  const signal = controller.signal

  return {
    abort: () => controller.abort(),
    ready: fetch(request, { ...options, signal }).then(res => res.json())
  }
}

export const api = {
  abortableGet (url) {
    return abortableFetch(url, {
      method: 'GET'
    })
  },
  async get (url) {
    return fetch(url, {
      method: 'GET',
    }).then(res => {
      return res.json()
    })
  },
  async post (url, body) {
    return fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    }).then(async (data) => {
      try {
        return await data.json()
      } catch (e) {

      }
    })
  },
  async patch (url, body) {
    return fetch(url, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    }).then(async (data) => {
      try {
        return await data.json()
      } catch (e) {

      }
    })
  }

}
