export const ordersListStore = {
  namespaced: true,

  state () {
    return {
      list: [],
      count: 0,
      loading: false,
    }
  },

  getters: {
    listGetter (state) {
      return state.list
    },
    countGetter (state) {
      return state.count
    },
    loadingGetter (state) {
      return state.loading
    },
  },

  mutations: {
    listUpdate (state, payload) {
      state.list = payload.list
      state.count = payload.count
    },
  },

  actions: {
    async fetchOrders ({ commit }) {
      const response = {
        data: [],
        count: 0,
      }

      commit({
        type: 'listGetter',
        list: response.data,
        count: response.count,
      })
    }
  },
}
