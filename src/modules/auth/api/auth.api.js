import { api } from '../../core/request/fetch'

export const authApi = {
  createAccount (request) {
    return api.post('http://localhost:4002/api/v1/auth/account', request)
  }
}
