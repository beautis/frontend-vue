import Vue from 'vue'
import { Plugin } from 'vue-fragment'
import VueMask from 'v-mask'
import Vuelidate from 'vuelidate'

import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import { createStore } from './store'
import './components/index'
import './assets/styles/main.scss'
import * as filters from './filters'

Vue.config.productionTip = false

Vue.use(VueMask)
Vue.use(Vuelidate)
Vue.use(Plugin)

const store = createStore()

for (const key in filters) {
  Vue.filter(key, filters[key])
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
