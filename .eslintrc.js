module.exports = {
  root: true,
  env: {
    node: true,
    jest: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    indent: 'off',
    'vue/script-indent': ['warn', 2, {
      baseIndent: 1
    }],
    'comma-dangle': 'off',
    'padded-blocks': 'off',
    eqeqeq: 'off',
  },
  overrides: [
    {
      files: [
        '**/components/**/*.{j,t}s?(x)',
        '**/modules/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
